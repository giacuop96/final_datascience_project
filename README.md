# Final_DataScience_Project

This is the project of DataScience in Lifescience class of SoSe23.

## Introduction
In this project, our aim is predicting survival and non-survival patients based on CNV data and transcriptomic data. Besides that, with the help of the machine learning model, we also want to find a subset of important genes, which has highest impact on patient's mortality, and its function under bladder cancer context.

## Original data
We stored the original data in data directory.
## CNV analysis
For running CNV analysis, you can go through the cnv_analysis.Rmd script in the code directory.\
The required packages for CNV analysis is GAIA. It is an old package so you need to downgrade your Bioconductor version. \

## Gene expression analysis
For running gene expression analysis, you can go through gene_expression_analysis.Rmd \
The required packages for gene expression analysis is DESeq2 and data.table. \

## Apply machine learning model
For running apply machine learning model, you can go through ml_model.ipynb \

## Results
Result data from each step of the project are in results directory. 

## Code junk
For the analysis that we didn't include into the presentation, the scripts are stored in the following git repository: \
https://github.com/giacuong171/MultiOmic_Datascience \
https://github.com/matanatmammadli/MultiOmics_DataScience \
https://github.com/sahami14/MultiOmicsDataScience \